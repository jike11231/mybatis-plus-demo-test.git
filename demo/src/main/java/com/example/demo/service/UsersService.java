package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Users;
import java.util.List;

/**
 * @author shaohua.zhang
 */
public interface UsersService extends IService<Users> {
    List<Users> getByName(String name);
}

