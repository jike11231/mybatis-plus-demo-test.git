package com.example.demo.controller;

import com.example.demo.entity.Users;
import com.example.demo.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @description: UsersController
 * @author: 懒虫虫~
 */
@RestController
public class UsersController {
    @Autowired
    private UsersService usersService;

    public List<Users> getByName(String name){
        return usersService.getByName(name);
    }
}
