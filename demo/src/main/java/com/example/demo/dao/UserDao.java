package com.example.demo.dao;

import com.example.demo.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author shaohua.zhang
 */
@Repository
public interface UserDao {
    /**
     * 根据主键删除实体类User
     * @param id
     * @return int
    */
    int deleteByPrimaryKey(Long id);
    /**
     * 插入实体类User
     * @param record
     * @return int
     */
    int insert(User record);
    /**
     * 根据主键插入实体类User
     * @param record
     * @return int
     */
    int insertSelective(User record);
    /**
     * 根据主键查询实体类User
     * @param id
     * @return User
     */
    User selectByPrimaryKey(Long id);
    /**
     * 根据所选择的主键更新实体类User
     * @param record
     * @return int
     */
    int updateByPrimaryKeySelective(User record);
    /**
     * 根据主键更新实体类User
     * @param record
     * @return int
     */
    int updateByPrimaryKey(User record);
}