package com.example.demo.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * user
 * @author 
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    /**
     * 主键ID
     *      @TableId(type = IdType.AUTO)//主键自动增长
     *      @TableId(type = IdType.ID_WORKER)//mp自带策略，生成19位的ID值，数字类型使用这种策略，比如long
     *      @TableId(type = IdType.ID_WORKER_STR)//mp自带策略，生成19位的ID值，字符串类型使用这种策略，比如string
     *      @TableId(type = IdType.INPUT)//ID值不会帮我们生成，需要自己手动输入ID
     *      @TableId(type = IdType.NONE)//不用任何策略，也是需要自己手动输入ID
     *      @TableId(type = IdType.UUID)//每次帮我们生成一个随机的唯一的ID值
     */
    @TableId(type = IdType.AUTO)//主键自动增长
    private Long id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 版本号，用于实现乐观锁(这个一定要加)
     * 添加这个注解是为了在后面设置初始值，不加也可以
    */
    @Version
    @TableField(fill = FieldFill.INSERT)
    private Integer version;

    /**
     * 创建日期
     * INSERT的含义就是添加，也就是说在做添加操作时，下面一行中的createTime会有值
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新日期
     * INSERT_UPDATE的含义就是在做添加和修改时下面一行中的updateTime都会有值，
     * 因为是第一次添加，还没有做修改（一般都使用这个）
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 删除标志
     */
    @TableLogic
    private Integer deleted;

    private static final long serialVersionUID = 1L;
}