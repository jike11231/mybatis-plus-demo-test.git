package com.example.demo.entity;

import java.util.List;

import com.example.demo.utils.TreeEntity;
import lombok.Data;

@Data
public class Menu implements TreeEntity<Menu> {
    public String id;
    public String name;
    public String parentId;
    public List<Menu> childList;
}