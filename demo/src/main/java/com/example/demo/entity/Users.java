package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用户实体对应表 users
 * @author shaohua.zhang
 */
@Data
@Accessors(chain = true)
public class Users {
    @TableId(type = IdType.AUTO)//主键自动增长
    private Long id;
    /**
     * 租户 ID
     */
    private Long tenantId;
    private String name;


    @TableField(exist = false)
    private String addrName;

}
