package com.example.demo.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.Users;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * MP 支持不需要 UsersMapper.xml 这个模块演示内置 CRUD 咱们就不要 XML 部分了
 * @author shaohua.zhang
 */
public interface UsersMapper extends BaseMapper<Users> {

    /**
     * 自定义SQL：默认也会增加多租户条件
     * 参考打印的SQL
     * @return
     */
    Integer myCount();

    /**
     * 根据username获取users用户地址
     * @param username
     * @return List<Users>
    */
    List<Users> getUserAndAddr(@Param("username") String username);

    /**
     * 根据name获取user_addr中地址用户
     * @param name
     * @return List<Users>
    */
    List<Users> getAddrAndUser(@Param("name") String name);
}
