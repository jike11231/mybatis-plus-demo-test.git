package com.example.demo.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.User;

/**
 * @author shaohua.zhang
 */
public interface UserMapper  extends BaseMapper<User> {
}
