package com.example.demo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.entity.User;
import com.example.demo.repository.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class DemoApplicationTests {
	@Resource
	UserMapper userMapper;

	@Test
	public void testAdd() {
		User user = new User();
		user.setName("OrangeTest17");
		user.setAge(22);
		user.setEmail("1525@qq.com");
		int insert = userMapper.insert(user);
		System.out.println(insert);
	}

	/**
	 * 乐观锁实现方式：
	 * 1、取出记录时，获取当前version
	 * 2、更新时，带上这个version
	 * 3、执行更新时， set version = newVersion where version = oldVersion
	 * 4、如果version不对，就更新失败
	 */
	//测试乐观锁，看version的值是否加1
	@Test
	void testOptimisticLocker(){
		//根据id查询数据
		User user = userMapper.selectById(2);
		System.out.println(user.getVersion());
		//进行修改
		user.setAge(21);
		userMapper.updateById(user);
	}

	//测试乐观锁，模拟失败场景
	@Test
	void testOptimisticLockerFail(){
        //线程1
		//根据id查询数据
		User user1 = userMapper.selectById(3);
		user1.setAge(20);

		//线程2
		User user2 = userMapper.selectById(3);
		user2.setAge(28);

		//模拟另一个线程执行了插队操作
		userMapper.updateById(user2);
		userMapper.updateById(user1);
	}

	@Test
	void testSelect(){
		QueryWrapper queryWrapper = new QueryWrapper();
		List<User> users = userMapper.selectList(queryWrapper);
		users.stream().forEach(i ->{
			System.out.println(i.getName());
		});
	}

	@Test
	void testSelectPage(){
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.like("name","OrangeTest");
		IPage<User> page = new Page<>(1,3);
		IPage<User> userPage = userMapper.selectPage(page,queryWrapper);
		System.out.println(userPage.getRecords());
		userPage.getRecords().stream().forEach(i ->{
			System.out.println(i.getId()+"\t"+i.getName());
		});
	}


}
