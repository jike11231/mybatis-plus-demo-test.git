package com.example.demo;

import com.example.demo.config.RequestDataHelper;
import com.example.demo.entity.NewUser;
import com.example.demo.repository.NewUserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import javax.annotation.Resource;
import java.util.HashMap;

/**
 * <p>
 * 内置 动态表名 演示
 * </p>
 *
 */
@SpringBootTest
class DynamicTableNameTest {
    @Resource
    private NewUserMapper newUserMapper;

    @Test
    void test() {
        // 自己去观察打印 SQL 目前随机访问 newuser_2021  newuser_2022 表
        for (int i = 0; i < 6; i++) {
            NewUser user = newUserMapper.selectById(1);
            System.err.println(user.getName());
        }
    }
}
