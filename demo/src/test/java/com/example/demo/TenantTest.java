package com.example.demo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.demo.entity.Users;
import com.example.demo.repository.UsersMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import javax.annotation.Resource;
import java.util.List;

/**
 * 多租户 Tenant 演示
 */
@SpringBootTest
public class TenantTest {
    @Resource
    private UsersMapper mapper;

    @Test
    public void testInsert() {
        Users user = new Users();
        user.setName("三丰丰");
        Assertions.assertTrue(mapper.insert(user) > 0);
        user = mapper.selectById(user.getId());
        Assertions.assertTrue(1 == user.getTenantId());
    }


    @Test
    public void testDelete() {
        Assertions.assertTrue(mapper.deleteById(3L) > 0);
    }


    @Test
    public void testUpdate() {
        Assertions.assertTrue(mapper.updateById(new Users().setId(1L).setName("mp")) > 0);
    }


    /*
    * 多个租户共用同一套表通过sql语句级别来隔离不同租户的资源，
    * 比如设置一个租户标识字段，每次查询的时候在后面附加一个筛选条件：TenantId=xxx。
    * 这样能低代价、简单地实现多租户服务，但是每次执行sql的时候需要附加字段隔离，否则会出现数据错乱。
      此隔离过程应该自动标识完成，
    * */
    @Test
    public void testSelect() {
        List<Users> userList = mapper.selectList(null);
        userList.forEach(u -> Assertions.assertTrue(1 == u.getTenantId()));
    }

    /**
     * 自定义SQL：默认也会增加多租户条件
     * 参考打印的SQL
     */
    @Test
    public void manualSqlTenantFilterTest() {
        System.out.println(mapper.myCount());
    }

    @Test
    public void testTenantFilter(){
        mapper.getAddrAndUser(null).forEach(System.out::println);
        mapper.getAddrAndUser("add").forEach(System.out::println);
        mapper.getUserAndAddr(null).forEach(System.out::println);
        mapper.getUserAndAddr("J").forEach(System.out::println);
    }

    //防止全表更新与删除插件
    @Test
    public void deleteAll() {
        //执行会报错： com.baomidou.mybatisplus.core.exceptions.MybatisPlusException: Prohibition of full table deletion
        int delete = mapper.delete(null);
        System.out.println("删除成功"+delete);

        /* 改成根据条件删除，就不会报错*/
        QueryWrapper<Users> queryWrapper = new QueryWrapper();
        Users user = new Users();
        user.setName("三丰丰");
        queryWrapper.lambda().eq(Users::getName,"三丰丰");
        mapper.delete(queryWrapper);
        int delete2 = mapper.delete(null);
        System.out.println("删除成功"+delete2);

    }

    @Test
    public void updateAll() {
        LambdaUpdateWrapper<Users> updateWrapper = new LambdaUpdateWrapper<Users>().set(Users::getName, "三丰丰");
        mapper.update(null, updateWrapper);
        System.out.println("修改成功");
    }


}
