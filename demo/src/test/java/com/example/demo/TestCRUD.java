package com.example.demo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.controller.UsersController;
import com.example.demo.entity.User;
import com.example.demo.entity.Users;
import com.example.demo.service.impl.UsersServiceImpl;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
* 测试 Service CRUD 接口
*/
@SpringBootTest
public class TestCRUD {

    @Autowired
    private UsersServiceImpl usersService;

    @Test
    public void test01() {
        // 插入一条记录（选择字段，策略插入）
        Users user1 = new Users();
        user1.setId(0L);
        user1.setName("Alex");
        user1.setAddrName("shenzhen");
        user1.setTenantId(1L);
        usersService.save(user1);
        // TableId 注解存在更新记录，否插入一条记录
        //userService.saveOrUpdate(user1);
    }

    @Test
    public void test02() {
        // 根据 ID 删除
        usersService.removeById(0L);
    }

    @Test
    public void test03() {
        IPage<Users> page = new Page<>(1,2);
        IPage<Users> tasks = usersService.page(page);
        System.out.println(tasks.getPages());
        System.out.println(tasks.getCurrent());
        System.out.println(tasks.getRecords());
        System.out.println(tasks.getTotal());
    }

    @Test
    public void test04() {
        System.out.println(usersService.getByName("Jone"));
    }

    @Test
    public void test05() {
        //System.out.println(usersController.getByName("Jone"));
        List<String> stringList = Lists.newArrayList();
        for (int i = 0; i < 10; i++) {
            stringList.add(String.valueOf(i));
        }

        List<Long> longList = stringList.stream().map(Long::valueOf).collect(Collectors.toList());
        System.out.println(longList);
    }

    @Test
    public void test06() {
        Users user1 = new Users();
        user1.setId(0L);
        user1.setName("Alex");
        user1.setAddrName("shenzhen");
        user1.setTenantId(1L);

        Users user2 = new Users();
        user2.setId(1L);
        user2.setName("Alex2");
        user2.setAddrName("shenzhen2");
        user2.setTenantId(2L);

        List<Users> listData = new ArrayList();
        listData.add(user1);
        listData.add(user2);

        List<User> list = listData.stream().map(v -> {
                                            User user = new User();
                                            user.setId(v.getId());
                                            user.setName(v.getName());
                                            return user;
                                        }).collect(Collectors.toList());

        list.forEach(System.out::println);
    }



}
